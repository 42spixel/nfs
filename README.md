# **Nfs**

Nfs server for spixel.

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/nfs/master/LICENSE.md)**.
