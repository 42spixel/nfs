FROM debian:latest

# Metadata
LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="Nfs server for Docker."

# Docker image name
ENV NAME=nfs

# Debian env
ENV DEBIAN_FRONTEND=noninteractive

# Update system and install packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
	iputils-ping \
	nfs-kernel-server \
	kmod \
	libcap2-bin

# Clean system
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Clean default config for nfs
RUN rm -v /etc/idmapd.conf /etc/exports

# Nfsv4 config
RUN mkdir -p /var/lib/nfs/rpc_pipefs
RUN mkdir -p /var/lib/nfs/v4recovery
RUN echo "rpc_pipefs  /var/lib/nfs/rpc_pipefs  rpc_pipefs  defaults  0  0" >> /etc/fstab
RUN echo "nfsd        /proc/fs/nfsd            nfsd        defaults  0  0" >> /etc/fstab

# Copy entrypoint and add right permissions 
COPY entrypoint.sh /usr/local/bin 
RUN chmod 755 /usr/local/bin/entrypoint.sh

# Timezone
ENV TZ="Europe/Paris"

# User
RUN useradd -ms /bin/bash docker

# Home
ENV HOME=/home/docker

# Permissions
RUN chown -R docker:docker $HOME

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# Change user
USER docker

# Entrypoint
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
